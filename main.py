# pylint: disable=C0116,W0613
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import json
import logging

from telegram import Update, ReplyKeyboardMarkup, ParseMode
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

from Content import Category, LENGTH, Contents
from SECRET import secret_key

import http.server
import threading
import os

# Enable logging

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)
selected = set()
selected_lengths = set()
contents = None

hebrew = {
    "COMEDY": "קומדיה 😂",
    "ACTION": "אקשן 😎",
    "POETRY_SLAM": "פואטרי סלאם 🎤",
    "KAN": "כאן 👇",
    "TED": "טד 👨‍🏫",
    "DOCUMENTARY": "דוקו 📄",
    "MAKO": "מאקו 📺",
    "VERY_SHORT": "קצר מאוד",
    "SHORT": "קצר",
    "MEDIUM": "בינוני",
    "LONG": "ארוך",
    "VERY_LONG": "ארוך מאוד",
    "GENERATE": "הדבר הבא! 🤯"
}

english = {v: k for k, v in hebrew.items()}


# Define a few command handlers. These usually take the two arguments update and
# context.
def start(update: Update, context: CallbackContext) -> None:
    """Starts the conversation"""
    global contents
    update.message.reply_text(
        "שלום וברוכים הבאים לבוט \"הדבר הבא\"! אני אהיה הבוט שלכם להיום והתפקיד שלי בעולם הוא פשוט: 😄"
    )
    update.message.reply_text(
        "אתם אומרים לי מה הסגנון של הדבר הבא שאתם רוצים לראות, ואני אומר לכם מה לראות! 🤯"
    )
    update.message.reply_text(
        "בהמשך אני אבקש מכם לבחור קטגוריות. תוכלו לבחור 0 או יותר קטגוריות ו0 או יותר זמנים. אני אחפש משהו שמתאים "
        "לכמה שיותר קטגוריות באחד הזמנים שבחרתם 😌"
    )
    with open("my_contents.json", "r", encoding='utf-8') as f:
        x = json.load(f)
        contents = Contents(x['contents'])
    return show_categories_keyboard(update, context)


def show_categories_keyboard(update: Update, context: CallbackContext) -> None:
    categories = [hebrew[e.name] + "\n✅" if e.name in selected else hebrew[e.name] for e in Category]
    lengths = [hebrew[e.name] + "\n✅" if e.name in selected else hebrew[e.name] for e in LENGTH]
    # categories_parts = list(np.array(categories).reshape((2, -1)))
    categories_parts = [categories[:4], categories[4:]]
    reply_keyboard = [categories_parts[0], categories_parts[1], lengths, [hebrew['GENERATE']]]
    # reply_keyboard = list(np.array(reply_keyboard).reshape((2, -1)))
    update.message.reply_text(
        'בחרו בבקשה קטגוריה. כשאתם מוכנים, לחצו על \"הדבר הבא\" ☺️',
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=False
        )
    )


def handle_answer(update: Update, context: CallbackContext) -> None:
    """handles the user message."""
    answer_str = english[update.message.text.split("\n")[0]]
    if answer_str == "GENERATE":
        return handle_generate(update, context)
    if answer_str in selected:
        selected.remove(answer_str)
    else:
        selected.add(answer_str)
    show_categories_keyboard(update, context)


def handle_generate(update: Update, context: CallbackContext) -> None:
    categories = [Category[c.upper()] for c in selected if c.upper() in Category._member_names_]
    lengths = [LENGTH[c.upper()] for c in selected if c.upper() in LENGTH._member_names_]
    result = contents.get_next(categories, lengths)
    update.message.reply_text(result, parse_mode=ParseMode.MARKDOWN_V2)

def run_http_server():
    port = int(os.environ.get("PORT", 8080))
    server_address = ('', port)
    httpd = http.server.HTTPServer(server_address, http.server.SimpleHTTPRequestHandler)
    httpd.serve_forever()

def main() -> None:
    # Start HTTP server in a separate thread
    threading.Thread(target=run_http_server, daemon=True).start()

    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater(secret_key)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))

    # on non command i.e message - echo the message on Telegram
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, handle_answer))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
