#!/usr/bin/env python
# pylint: disable=C0116,W0613
# This program is dedicated to the public domain under the CC0 license.

"""
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import json
import logging
from os.path import isfile

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update, ForceReply
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)

# Enable logging
from Content import Category, LENGTH
from main import hebrew, english

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

URL, NAME, CATEGORIES = range(3)

content_url = None
content_name = None
content_categories = set()
content_length = 'VERY_SHORT'


def start(update: Update, context: CallbackContext) -> int:
    """Starts the conversation and asks the user about url."""

    update.message.reply_text(
        'Hi! My name is Contents adder bot. I will add contents for you. '
        'Send /cancel to stop talking to me.\n\n'
        'What is the url of the content?'
    )

    return URL


def handle_url(update: Update, context: CallbackContext) -> int:
    """Stores the url and asks for name"""
    global content_url
    content_url = update.message.text
    update.message.reply_text(
        'I see! Please send me a name for the content',
    )

    return NAME


def handle_name(update: Update, context: CallbackContext) -> int:
    """Stores the name and asks for categories."""
    global content_name, content_categories
    content_name = update.message.text
    update.message.reply_text(
        'Thanks'
    )
    show_categories_keyboard(update, context)
    return CATEGORIES


def show_categories_keyboard(update: Update, context: CallbackContext):
    categories = [hebrew[e.name] + "\n✅" if e.name in content_categories else hebrew[e.name] for e in Category]
    lengths = [hebrew[e.name] + "\n✅" if e.name == content_length else hebrew[e.name] for e in LENGTH]
    categories_parts = [categories[:4],categories[4:]] 
    reply_keyboard = [categories_parts[0], categories_parts[1], lengths, [hebrew['GENERATE']]]
    update.message.reply_text(
        'Great! Now, please select categories relevant for this content',
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=False
        )
    )


def handle_categories(update: Update, context: CallbackContext) -> int:
    """Stores the categories and """
    global content_length, content_categories
    selected = english[update.message.text.split("\n")[0]]
    if selected == 'GENERATE':
        return handle_generate(update, context)
    else:
        if selected in Category.__members__:
            if selected in content_categories:
                content_categories.remove(selected)
            else:
                content_categories.add(selected)
        else:
            content_length = selected
        show_categories_keyboard(update, context)
        return CATEGORIES


def handle_generate(update: Update, context: CallbackContext) -> int:
    """yields python code for the content"""
    global content_url, content_name, content_categories, content_length
    if len(content_categories) == 0:
        update.message.reply_text(
            'You must select 1 or more categories'
        )
        return CATEGORIES
    result = {
        "name": content_name,
        "url": content_url,
        "length": content_length,
        "categories": list(content_categories)
    }
    # update.message.reply_text(
    #     str(result)
    # )
    if not isfile("my_contents.json"):
        with open("my_contents.json", "w", encoding="utf-8") as my_contents_file:
            empty_contents = {
                "contents": []
            }
            json.dump(empty_contents, my_contents_file, indent=4, ensure_ascii=False)
    with open("my_contents.json", "r", encoding="utf-8") as my_contents_file:
        # First we load existing data into a dict.
        contents_json = json.load(my_contents_file)
        # Join new_data with file_data inside emp_details
        contents_json["contents"].append(result)
    with open("my_contents.json", "w", encoding="utf-8") as my_contents_file:
        # Sets file's current position at offset.
        my_contents_file.seek(0)
        # convert back to json.
        json.dump(contents_json, my_contents_file, indent=4, ensure_ascii=False)
    return start(update, context)


def cancel(update: Update, context: CallbackContext) -> int:
    """Cancels and ends the conversation."""
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text(
        'Bye! I hope we can talk again some day.', reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END


def main() -> None:
    """Run the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater("5052942089:AAF_VUF6Q3xIhvOgLrqzxSO9yYjjqdEm7mk")

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler with the states URL, NAME, CATEGORIES and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            URL: [MessageHandler(Filters.text & ~Filters.command, handle_url)],
            NAME: [MessageHandler(Filters.text & ~Filters.command, handle_name)],
            CATEGORIES: [MessageHandler(Filters.text & ~Filters.command, handle_categories)]
        },
        fallbacks=[CommandHandler('cancel', cancel)],
    )

    dispatcher.add_handler(conv_handler)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()

