import re
from enum import Enum, auto


def escape_markdown(text):
    # Use {} and reverse markdown carefully.
    parse = re.sub(r"([_*\[\]()~`>\#\+\-=|\.!])", r"\\\1", text)
    reparse = re.sub(r"\\\\([_*\[\]()~`>\#\+\-=|\.!])", r"\1", parse)
    return reparse


class Content:
    def __init__(self, content_dict):
        self._name = escape_markdown(content_dict['name'])
        self._url = escape_markdown(content_dict['url'])
        self._length = LENGTH[content_dict['length']]
        self._categories = list(map(lambda c: Category[c], content_dict['categories']))

    @property
    def url(self):
        return self._url

    @property
    def name(self):
        return self._name

    @property
    def categories(self):
        return self._categories

    @property
    def length(self):
        return self._length

    def __str__(self):
        return f"*{self.name}*\n\n{self.url}"

    def to_dict(self):
        return {
            "name": escape_markdown(self.name),
            "url": escape_markdown(self.url),
            "length": self.length.name,
            "categories": list(map(lambda x: x.name, self.categories))
        }


class Category(Enum):
    COMEDY = auto()
    ACTION = auto()
    POETRY_SLAM = auto()
    KAN = auto()
    TED = auto()
    DOCUMENTARY = auto()
    MAKO = auto()


class LENGTH(Enum):
    VERY_SHORT = auto()
    SHORT = auto()
    MEDIUM = auto()
    LONG = auto()
    VERY_LONG = auto()


class Contents:
    def __init__(self, contents=None):
        if contents is None:
            contents = []
        self._contents = list(map(lambda c: Content(c), contents))
        self._contents.reverse()

    def get_next(self, categories, lengths):
        next_content = None
        for k in range(len(set(categories)), -1, -1): # when k is zero we just look for something with appropriate length (which we always have based on my contents)
            for content in self._contents:
                if len(categories) == 0:
                    if len(lengths) == 0 or content.length in lengths:
                        next_content = content
                        break
                elif len(set(content.categories) & set(categories)) == k:
                    if len(lengths) == 0 or content.length in lengths:
                        next_content = content
                        break
            if next_content is not None: # found something
                break
        if next_content is None: # shouldnt reach here if we have at least one content per length
            next_content = self._contents[0] # default value
        self._contents.append(next_content)
        self._contents.remove(next_content)  # removes the first instance of content
        return str(next_content)

    def to_dict(self):
        return {
            "contents": list(map(lambda x: x.to_dict(), self._contents))
        }
